import React, { Component } from 'react';
import {
    StyleSheet, Text, View
    , Image, TextInput, TouchableOpacity
} from 'react-native';

export default class AboutUsScreen extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        let { name, gitlab, mail, profil, telegram } = this.props.route.params


        return (
            <View style={style.container}>
                <Image source={profil} style={style.roundImage}></Image>

                <View style={style.textLayout}>
                    <Text style={style.textName}>{name}</Text>
                </View>
                <View style={style.aboutContainer}>
                    <View style={style.detailAbout}>
                        <Image source={require('../images/email.jpg')} style={style.logo} />
                        <Text style={style.textAbout}>{mail}</Text>
                    </View>
                    <View style={style.detailAbout}>
                        <Image source={require('../images/gitlab.png')} style={style.logo} />
                        <Text style={style.textAbout}>{gitlab}</Text>
                    </View>
                    <View style={style.detailAbout}>
                        <Image source={require('../images/telegram.png')} style={style.logo} />
                        <Text style={style.textAbout}>{telegram}</Text>
                    </View>

                </View>
            </View>
        )
    }
}
const style = StyleSheet.create({
    textAbout: {
        color: '#grey',
        marginStart: 10,
        marginTop: 10,
        justifyContent: "center",
        textAlign: "center"
    },
    logo: {
        width: 41,
        height: 45
    },
    detailAbout: {
        flexDirection: "row",
        height: 75,
        marginTop: 10
    },
    aboutContainer: {
        height: 326,
        width: 318,
        backgroundColor: '#white',
        margin: 20,
        borderRadius: 5,
        padding: 20

    },
    roundImage: {
        height: 151,
        width: 146,
        borderRadius: 100,
        backgroundColor: '#81B4C6',
        justifyContent: "center",
        marginStart: 100,
        marginTop: 50
    }, textLayout: {
        height: 52,
        width: 255,
        borderRadius: 7,
        backgroundColor: '#81B4C6',
        justifyContent: "space-around",
        marginStart: 50,
        marginTop: 15
    }, textName: {
        color: '#ffffff',
        alignContent: "space-around",
        justifyContent: "space-around",
        textAlign: "center",
        fontSize: 20,
    },
    button: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
        borderRadius: 20,
        alignItems: 'center',
        width: '70%',
        marginHorizontal: 40,
        marginVertical: 20,
        backgroundColor: '#2B0AF3'
    },
    inputStyle: {
        width: '70%',
        marginVertical: 20,
        marginHorizontal: 40,
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        flexDirection: 'row'
    },
    imageLogo: {
        height: 110,
        width: 100,
        marginTop: 40
    },
    logoHeader: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        flex: 1,
        backgroundColor: '#D0F7FF'
    }
})