import React, { Component } from 'react';
import {
    StyleSheet, Text, View
    , Image, TextInput, TouchableOpacity,ScrollView
} from 'react-native';

export default class HalamanDetail extends Component {
    constructor(props){
        super(props)
        
    }
    render() {
        let movieDetail=this.props.route.params.item.item
        return (
            <ScrollView style={style.container}>
                <View >
                    <View style={style.top}>
                        <Image style={style.back} source={{uri: `http://image.tmdb.org/t/p/original${movieDetail.backdrop_path}`}}/>
                        <Image style={style.imageLayout} source={{uri: `http://image.tmdb.org/t/p/original${movieDetail.poster_path}`}}/>
                        <View style={style.judulLayout}>
                        <Text style={style.judul}>{movieDetail.title}</Text>

                        </View>
                    </View>
                    <View style={style.top2}>
                        <View style={style.rating}>
                            <Image source={require('../images/star.png')}style={style.star}/>
                            <Text style={style.textStar}>{movieDetail.popularity}</Text>
                        </View>
                        <View style={style.date}>
                            <Image source={require('../images/date.png')}style={style.star}/>
                            <Text style={style.textDate}>{movieDetail.release_date}</Text>
                        </View>                            
                    </View>
                    <View style={style.descLayout}>
                        <Text style={style.textDesc}>{movieDetail.overview}</Text>
                    </View>
                </View>

            </ScrollView>
            
        )
    }
}
const style = StyleSheet.create({
    judulLayout:{
        height:100,
        borderRadius:10,
        marginTop:180,
        marginStart:10,
        marginEnd:20,
        backgroundColor:'#81B4C6'
    },
    textDesc:{
        padding:10,
        color:'#ffffff',
        fontSize:17,
        textAlign:"justify"
    },  
    descLayout:{
        margin:12,
        width:332,
        height:344,
        backgroundColor:'#81B4C6',
        borderRadius:7
    },  
    textDate:{
        marginStart:20,
        marginTop:5,
        color:'#ffffff'
    },
    date:{
        marginStart:20,
        padding:5,
        flexDirection:"row",
        width:190,
        height:40,
        backgroundColor:'#81B4C6',
        borderRadius:5
    },
    textStar:{
        marginStart:10,
        margin:5,
        color:'#ffffff'
    },  
    rating:{
        padding:5,
        flexDirection:"row",
        width:120,
        height:40,
        backgroundColor:'#81B4C6',
        borderRadius:5
    },
    star:{
        height:29,
        width:29
    },
    top2:{
        marginStart:20,
        marginTop:20,
        flexDirection:"row"
    },
    top:{
        flexDirection:"row"
    },
    back:{
        position:"absolute",
        height:220,
        width:360,
        backgroundColor:'#81B4C6'
    },
    item:{
        fontSize:18,
        color:'#4B707F',
        textAlign:"justify"
    },
    judul:{
        width:160,
        marginStart:10,
        marginEnd:10,
        fontSize:20,
        color:'#ffffff',
        
    },
    itemLayout:{
        borderRadius:5,
        backgroundColor:'#A6DCEF',
        marginStart:20,
        marginEnd:20,
        marginTop:10,
        marginBottom:20,
        padding:10,
        justifyContent:"center",
        alignContent:"center"
    },
   
    imageLayout: {
        height:231,
        width:154,
        backgroundColor:'#A6DCEF',
        borderRadius:10,
        marginStart:10,
        marginTop:120
    },
    container: {
        flex: 1,
        backgroundColor: '#grey'
    }
})