import React,{Component} from 'react'
import {View,StyleSheet,Text,Image,TouchableOpacity, TextInput, FlatList, findNodeHandle,
    ActivityIndicator
} from 'react-native'
import Icon from "react-native-vector-icons/MaterialIcons";
import genreList from "../common/Genre.json";
import {connect} from "react-redux";
import axios from "axios";
const objectMovie= {
    "popularity": 467.358,
    "vote_count": 1844,
    "video": false,
    "poster_path": "/s1cVTQEZYn4nSjZLnFbzLP0j8y2.jpg",
    "id": 8619,
    "adult": false,
    "backdrop_path": "/m11Mej9vbQqcXWgYrgPboCJ9NUh.jpg",
    "original_language": "en",
    "original_title": "Master and Commander: The Far Side of the World",
    "genre_ids": [
        12,
        18,
        10752
    ],
    "title": "Master and Commander: The Far Side of the World",
    "vote_average": 7,
    "overview": "After an abrupt and violent encounter with a French warship inflicts severe damage upon his ship, a captain of the British Royal Navy begins a chase over two oceans to capture or destroy the enemy, though he must weigh his commitment to duty and ferocious pursuit of glory against the safety of his devoted crew, including the ship's thoughtful surgeon, his best friend.",
    "release_date": "2003-11-14"
}
class MovieItem extends Component{
    constructor(props){
        super(props)
    }
    
    getGenreString(genre_ids){
        let strGenre="";
        for(let i=0;i<genre_ids.length-1;i++){
            // strGenre+=`${genre_ids[i]}`
            if(genreList.genres[`${genre_ids[i]}`])
                strGenre+=genreList.genres[`${genre_ids[i]}`]+", ";
        }
        if(genreList.genres[`${genre_ids[genre_ids.length-1]}`])
            strGenre+=genreList.genres[`${genre_ids[genre_ids.length-1]}`];
        return strGenre
    }
    render(){
        return(
            <View style={style.movieItem}>
                        <Image source={{uri: `http://image.tmdb.org/t/p/w185${this.props.movie.item['poster_path']}`}}
                         style={style.movieImage}  />
                        <View style={style.movieDesc}>
                            <Text style={style.movieTitle}>{this.props.movie.item['title']}</Text>
                            <Text style={style.movieGenre}>{this.getGenreString(this.props.movie.item['genre_ids'])}</Text>
                            <View style={style.rating}>
                                <Icon name="stars" size={20}/>
                                <Text style={{marginLeft:10}}>{this.props.movie.item['popularity']}</Text>
                            </View>
                            <TouchableOpacity style={style.movieButton} onPress={this.props.goToDetail}>
                                <Icon name="visibility" style={{color:'white', marginTop:4,marginRight:10}}></Icon>
        <Text style={{color:'white'}}>Detail </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
        )
    }
}
class MainMenuScreen extends Component{
    constructor(props){
        super(props)
        this.state={
            url:`https://api.themoviedb.org/3/discover/movie?api_key=b97b0d4dc7e60aaefb9ae3e521726652&sort_by=popularity.desc&page=`,
            query:'',
            isSearchedMode:false
        
        }
    }
    fetchDataApi(page=1){
        if(page>this.props.page){
            this.props.fetching();
            axios.get(`${this.state.url}${page}`).then(result=>{
                this.props.addData(result.data.results)
            }).catch(err=>{
                console.log("error");
            })
        }
        
    }
    componentDidMount(){
        this.fetchDataApi();
    }
    refresh(){
        this.props.refreshing();
        this.fetchDataApi(this.props.page)
    }
    multipleFlatList(){
        let array=[];
        for(let i=0;i<this.props.data.length;i++){
            let flatlist=(<MovieItem movie={{item:this.props.data[i]}}/>)
            array.push(flatlist)
        }
        return array;
    }
    renderFooter(){
        return(<View style={{marginTop:20,alignItems:'center'}}>
            <ActivityIndicator size="large"/>
        </View>)
    }
    onSearch(){
        if(this.state.query!=""){
            this.props.returnPage()
            this.setState({
                url:`https://api.themoviedb.org/3/search/movie?api_key=b97b0d4dc7e60aaefb9ae3e521726652&query=${this.state.query}&page=`
                ,isSearchedMode:true
            },()=>{
                this.fetchDataApi(this.props.page+1)
            })
        }
    }
    goToDetail(movie){
        this.props.navigation.push("Detail",{item:movie})
    }
    render(){
        // console.log(this.props.data);
        return(
            <View style={style.container}>
               <View style={style.searchHeader}>
                   <Icon name="search" size={25} onPress={()=>this.onSearch()} style={{margin:10}}/>
                   <TextInput
                   onChangeText={(text)=>{
                    this.setState({query:text})
                    if(text==""){
                         this.props.returnPage();
                         this.setState({url:`https://api.themoviedb.org/3/discover/movie?api_key=b97b0d4dc7e60aaefb9ae3e521726652&sort_by=popularity.desc&page=`,
                                        isSearchedMode:false
                        },
                         ()=>{this.fetchDataApi(this.props.page+1)}
                         )
                       
                    }
                   }} 
                   value={this.state.query}
                    placeholder="Search Movies" placeholderTextColor="black" style={{marginLeft:10}}></TextInput>
                </View>
                <View style={style.movieList}>
                <Text style={{marginVertical:10,marginLeft:40, fontFamily:"Roboto",fontSize:20}}>{this.state.isSearchedMode?`Searched Movies With Key "${this.state.query}"`:"Trending Now"}</Text>
                
                   <FlatList 
                    
                    data={this.props.data}
                    renderItem={(movie)=>{
                        return <MovieItem movie={movie} goToDetail={()=>this.goToDetail(movie)}/>
                    }}
                    keyExtractor={(movie,index)=>{return movie.id.toString()}}
                    onEndReachedThreshold={0.5}
                    onEndReached={(distance)=>{
                        this.fetchDataApi(this.props.page+1)
                    }}
                    ListFooterComponent={this.renderFooter}
                    />
                </View>

            </View>
        )
    }
}
const style=StyleSheet.create({
    movieButton:{
        flexDirection:'row',
        padding:10,
        backgroundColor:'blue',
        width:'50%',
        justifyContent:'center',
        borderRadius:10,
        marginTop:10
    },
    rating:{
        flexDirection:"row",
        marginTop:10,
    },
    movieGenre:{
        fontFamily:"Roboto",
        fontSize:15,
        fontWeight:'100',
        marginTop:5,
    },
    movieTitle:{
        fontSize:18,
        fontWeight:'bold',

    },
    movieDesc:{
        paddingTop:10,
        width:'50%',
        marginLeft:-20,
    },
    movieImage:{
        width:200,
        height:200,
        resizeMode:'contain',
        borderRadius:10,
    },
    movieItem:{
        flexDirection:'row',
        marginVertical:10,
    },
    movieList:{
        flex:1,
        marginTop:20,
        marginRight:5
    },
    searchHeader:{
        marginTop:40,
        marginLeft:40,
        flexDirection:'row',
        height:50,
        width:300,
        borderRadius:20,
        backgroundColor:'#DCD3D3'
    },
    container:{
        
        flex:1,
    }
})
const stateProp=(state)=>{
    return{
        data:state.data,
        page:state.page,
        isLoading:state.isLoading
    }
}
const actionProp=()=>{
    return {
        fetching:() =>{return {type:"fetch"}},
        addData:(data)=>{return{type:'addFetch',dataApi:data}},
        refreshing:()=>{return{type:'refreshed'}},
        returnPage:()=>{return{type:'Page'}}
    }
}
export default connect(stateProp,actionProp())(MainMenuScreen)