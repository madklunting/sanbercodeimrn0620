import React, { Component, isValidElement } from 'react';

import {connect} from "react-redux";
import {profil} from "../common/User";
import {
    StyleSheet, Text, View
    , Image, TextInput, TouchableOpacity
} from 'react-native';

class LoginScreen extends Component {
    constructor(props){
        super(props)
        this.state={
            username:"",
            password:"",
            failedLogin:false
        }
    }
    login(){
        const result=profil.Profils.filter((val)=>val.username==this.state.username && val.password==this.state.password)
        if(result.length==1){
            this.props.LoginTo();
        }else{
            this.setState({
                failedLogin:true
            })
        }
    }
    render() {
        if(this.props.isLogin)
            this.props.navigation.push("Menu")
        return (
            <View style={style.container}>
                <View style={style.logoHeader}>
                    <Image style={style.imageLogo} source={require("../images/legi.png")}></Image>
                    <Text style={{ fontSize: 40, fontFamily: 'Roboto' }}>Kasmad<Text style={{ fontWeight: 'bold' }}>Movie</Text></Text>
                </View>
                <View style={{ margin: 40 }}>
                    <Text style={{ fontFamily: 'Roboto', fontSize: 25 }}>Login</Text>
                </View>
                <View style={style.inputStyle}>
                    <Image source={require("../images/username.png")} ></Image>
                    <TextInput placeholderTextColor='grey' placeholder="username " style={{ marginHorizontal: 30 }}
                        onChangeText={(text)=>{
                            this.setState({
                                username:text
                            })
                        }}
                        value={this.state.username}
                    ></TextInput>
                </View>
                <View style={style.inputStyle}>
                    <Image source={require("../images/password.png")} ></Image>
                    <TextInput placeholderTextColor='grey' secureTextEntry={true} placeholder="password "
                    onChangeText={(text)=>{
                        this.setState({
                            password:text
                        })
                    }}
                    value={this.state.password}
                    style={{ marginHorizontal: 30 }}></TextInput>
                </View>
                <Text style={{textAlign:'center',color:'red'}}>{this.state.failedLogin?"Username atau Password Salah":""}</Text>
                <TouchableOpacity style={style.button} onPress={()=>this.login()}>
                    <Text style={{ padding: 10, color: 'white', fontFamily: 'Roboto', fontSize: 20 }}> Login</Text>
                </TouchableOpacity>                
            </View>
        )
    }
}
const style = StyleSheet.create({
    button: {
        shadowColor: "grey",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
        borderRadius: 20,
        alignItems: 'center',
        width: '70%',
        marginHorizontal: 40,
        marginVertical: 20,
        backgroundColor: '#808080'
    },
    inputStyle: {
        width: '70%',
        marginVertical: 20,
        marginHorizontal: 40,
        borderBottomWidth: 1,
        borderBottomColor: 'grey',
        flexDirection: 'row'
    },
    imageLogo: {
        height: 100,
        width: 100,
        marginTop: 40
    },
    logoHeader: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        flex: 1,
        backgroundColor: '#grey'
    }
})
const mapState=(state)=>{
    return{
        isLogin:state.isLogin
    }
}
const functionality=()=>{
    return{
        LoginTo:()=>({type:'login'})
    }
}
export default connect(mapState,functionality())(LoginScreen)