import React from 'react';
import { View,StyleSheet,Image, TouchableOpacity,Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export default class ListItem extends React.Component {
    render() {
        let list = this.props.list;
        return (
            <View style={styles.container}>
                <View style={styles.tabBar}>
                    <MaterialCommunityIcons name={list.iconName} style={styles.icon}/>
                    <View style={{marginLeft:20, width:180}}>
                        <Text style={{fontSize:20, fontWeight : 'bold', color : '#003366'}}>
                            {list.skillName}
                        </Text>
                        <Text style={{fontSize:15, fontWeight : 'bold', color : '#45abcf'}}>
                            {list.categoryName}
                        </Text>
                        <Text style={{fontSize:30, fontWeight : 'bold', color : 'white'}}>
                            {list.percentageProgress}
                        </Text>
                    </View>
                    <Icon name="keyboard-arrow-right" style={styles.icon}/>
                </View>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    container : {
        flex:0.2,
        backgroundColor:'#B4E9FF',
        padding: 10,
        margin: 5,
        borderRadius:10,
        justifyContent: "space-around",
    },
    tabBar:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    icon:{
        fontSize:70,
        color:'#003366'
    },
});