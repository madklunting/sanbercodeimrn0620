import React from 'react';
import { 
    View,
    StyleSheet,
    Image,
    Text,
    TouchableOpacity, 
    TextInput,
    ScrollView,
    FlatList
} from 'react-native';
import Constants from 'expo-constants';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ListItem from './components/ListItem';
import Data from './skillData.json';

export default class Register extends React.Component {
    render () {
        return (
            <View style={styles.container}>
                <View style={styles.logoBar}>
                    <Image source={require('./images/sanber.png')} style={{width:180, height:50}} />
                </View>
                <View style={styles.textBar}>
                    <Icon name="account-circle" size={38} color='#3EC6FF'/>
                    <View style={{marginLeft:5}}>
                        <Text style={{fontSize:15, fontWeight : 'bold', color : '#003366'}}>Hallo,</Text>
                        <Text style={{fontSize:18, fontWeight : 'bold', color : '#3EC6FF'}}>Abdul Handsome</Text>
                    </View>
                </View>
                <View style={styles.div}>
                    <Text style={{fontSize:20, fontWeight : 'bold', color : '#003366'}}>List Of Skill's</Text>
                    <View style={{borderBottomColor: '#3EC6FF',borderBottomWidth: 3,marginTop:5}}/>
                </View>
                <View style={styles.tabBar}>
                    <TouchableOpacity style={styles.button}>
                        <Text style={{fontWeight : 'bold', color : '#003366'}}>Library / Framework</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button}>
                        <Text style={{fontWeight : 'bold', color : '#003366'}}>Programming</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button}>
                        <Text style={{fontWeight : 'bold', color : '#003366'}}>Technology</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.body}>
                    <FlatList
                        data={Data.items}
                        renderItem={(list)=><ListItem list={list.item}/>}
                        keyExtractor={(item)=>item.id+""}
                        ItemSeparatorComponent={()=>
                            <View style={{height:0.1}}/>
                        }
                    />
                </View>
            </View>
        )
    } 
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: Constants.statusBarHeight,
      marginLeft:5,
      marginRight:5
    },
    logoBar: {
       alignItems : 'flex-end',
       height:60
    },
    textBar: {
        alignItems : 'flex-start',
        flexDirection : 'row'
    },
    tabBar:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    body:{
        flex:1
    },
    div: {
        marginTop : 15,
        marginBottom : 2
    },
    button: {
        backgroundColor: '#B4E9FF',
        padding: 8,
        margin: 5,
        borderRadius : 10,
    },
    input: {
        marginTop : 4,
        marginBottom: 12,
        marginLeft:15,
        marginRight:15,
        paddingLeft:15,
        paddingRight:15,
        height: 40,
        borderColor: '#003366',
        borderWidth: 1,
        borderRadius : 5
     },
     submitButton1: {
        backgroundColor: '#003366',
        padding: 10,
        margin: 15,
        width : 100,
        borderRadius : 10,
        alignItems : 'center'
     },
     submitButton2: {
        backgroundColor: '#3EC6FF',
        padding: 10,
        margin: 15,
        width : 100,
        borderRadius : 10,
        alignItems : 'center'
     },
     submitButtonText:{
        color: 'white',
        fontSize : 18,
        fontWeight : 'bold'
     }
  });