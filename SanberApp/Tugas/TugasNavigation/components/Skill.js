import React from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';

import Icon from '@expo/vector-icons/MaterialCommunityIcons'; 

export default class Note extends React.Component {
  render() {
    const {skill} = this.props

    return (
      <View key={skill.id} style={styles.skillContainer}>
        <View style={styles.skillIcon}>
          <Icon name={skill.iconName} size={72} color="#003366" />
        </View>

        <View style={styles.skillDetail}>
          <Text style={styles.skillName}>
            {skill.skillName}
          </Text>
          <Text style={styles.categoryName}>
            {skill.categoryName}
          </Text>
          <Text style={styles.percentage}>
            {skill.percentageProgress}
          </Text>
        </View>

        <View style={styles.chevronRight}>
          <Icon name='chevron-right' size={72} color="#003366" />
        </View>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  skillContainer: {
    backgroundColor: '#B4E9FF',
    padding: 16,
    marginBottom: 16,
    borderRadius: 8,
    shadowColor: '#333',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 0,
    flexDirection: 'row',
  },
  skillIcon: {
    flex: 1,
    textAlign: 'left',
  },
  skillDetail: {
    flex: 3,
    paddingHorizontal: 8,
  },
  skillName: {
    color: '#003366',
    fontWeight: 'bold',
    fontSize: 20,
  },
  categoryName: {
    color: '#3EC6FF',
  },
  percentage: {
    color: 'white',
    fontSize: 40,
    textAlign: 'right',
  },
  chevronRight: {
    flex: 1,
    textAlign: 'right',
  }
});

