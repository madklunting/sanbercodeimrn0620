import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import { LoginScreen, AboutScreen, SkillScreen, ProjectScreen, AddScreen } from './Screen'
// import LoginScreen from './LoginScreen'
// import AboutScreen from './AboutScreen'
// import SkillScreen from './SkillScreen'
// import ProjectScreen from './ProjectScreen'
// import AddScreen from './AddScreen'

const RootStack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tabs = createBottomTabNavigator();

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Skill" component={SkillScreen} options={{ title: 'Skill' }} />
    <Tabs.Screen name="Project" component={ProjectScreen} options={{ title: 'Proyek' }} />
    <Tabs.Screen name="Add" component={AddScreen} options={{ title: 'Tambah' }} />
  </Tabs.Navigator>
)
const DrawerScreen = () => (
  <Drawer.Navigator>
    <Drawer.Screen name="Tabs" component={TabsScreen} options={{ title: 'Home' }} />
    <Drawer.Screen name="About" component={AboutScreen} options={{ title: 'About Me' }} />    
  </Drawer.Navigator>
)

export default () => (
  <NavigationContainer>
    <RootStack.Navigator>
      <RootStack.Screen
        name="Login"
        component={LoginScreen}
        options={{ title: 'Login' }}
      />
      <RootStack.Screen
        name="Drawer"
        component={DrawerScreen}
        options={{ title: 'Home' }}
      />
    </RootStack.Navigator>
    
  </NavigationContainer>
);
