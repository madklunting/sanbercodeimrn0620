import React, { Component } from 'react'
import { View, StyleSheet, Image, TouchableOpacity, Text, Button, ScrollView, TextInput, FlatList } from 'react-native'

import Icon from '@expo/vector-icons/FontAwesome'; 
// import { FontAwesome } from '@expo/vector-icons'; 
export default class App extends Component {
  render() {
    const { navigation } = this.props;

    return (
      <ScrollView style={styles.container}>

        <View>
          <Text style={styles.aboutMe}>Tentang Saya</Text>
        </View>

        <View style={styles.userInfoContainer}>
          <View style={styles.userPhoto}>
            <Image
              source={require('./images/photo.png')}
              style={{width: 200, height: 200, borderRadius: 100}}
            />
          </View>         

          <View>
            <Text style={styles.username}>Muhammad Ichsan</Text>
            <Text style={styles.userInfo}>React Native Developer</Text>
          </View>          
        </View>

        {/* <View style={{marginVertical: 16, padding: 16}}>
          <Button title="Drawer" onPress={() => navigation.toggleDrawer()} />
        </View> */}

        <View style={styles.box}>
          <Text style={styles.boxTitle}>Portofolio</Text>
          <View style={{height:0.5, backgroundColor:'#003366', marginVertical: 4}} />

          <View style={styles.portofolioBox}>
            <TouchableOpacity style={styles.portofolioItem}>
              <Icon name="gitlab" size={36} style={{color: '#3EC6FF'}} />
              <Text style={styles.portofolioTitle}>@Madklunting</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.portofolioItem}>
              <Icon name="github" size={36} style={{color: '#3EC6FF'}} />
              <Text style={styles.portofolioTitle}>@madklunting</Text>
            </TouchableOpacity>

          </View>
        </View>

        <View style={styles.box}>
          <Text style={styles.boxTitle}>Hubungi Saya</Text>
          <View style={{height:0.5, backgroundColor:'#003366', marginVertical: 4}} />

          <View style={styles.contactBox}>
            <TouchableOpacity style={styles.contactItem}>
              <Icon name="facebook-square" size={36} style={{color: '#3EC6FF', padding: 8, flex: 0.3}} />
              <Text style={styles.contactTitle}>Muhammad Ichsan</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.contactItem}>
              <Icon name="instagram" size={36} style={{color: '#3EC6FF', padding: 8, flex: 0.3}} />
              <Text style={styles.contactTitle}>@madklunting</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.contactItem}>
              <Icon name="twitter" size={36} style={{color: '#3EC6FF', padding: 8, flex: 0.3}} />
              <Text style={styles.contactTitle}>@madklunting</Text>
            </TouchableOpacity>

          </View>
        </View>

        

      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  aboutMe: {
    color: '#003366',
    fontSize: 30,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  userInfoContainer: {
    padding: 20,
  },
  userPhoto: {
    paddingVertical: 20,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  username: {
    color: '#003366',
    fontSize: 24,
    textAlign: 'center',
    fontWeight: 'bold',
    paddingBottom: 8,
  },
  userInfo: {
    color: '#3EC6FF',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: 'bold',
    paddingBottom: 8,
  },
  box: {
    backgroundColor: '#EFEFEF',
    marginHorizontal: 16,
    padding: 16,
    borderRadius: 12,
    marginBottom: 16,
  },
  boxTitle: {
    color: '#003366',
  },

  portofolioBox: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  portofolioItem: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 16,
  },
  portofolioTitle: {
    color: '#003366',
    paddingTop: 4,
    fontWeight: 'bold',
  }, 

  contactBox: {
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  contactItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 'auto',
    justifyContent: 'center',
    paddingVertical: 8,
  },
  contactTitle: {
    color: '#003366',
    fontWeight: 'bold',
    width: 150,
    paddingLeft: 16,
  },   
})
