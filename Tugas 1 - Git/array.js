
console.log('Soal No. 1 (Range)','\n') 
function range(startNum, finishNum) {
  let result = []
  if (startNum && finishNum) {
    if (startNum < finishNum) {
      for(let i = startNum ; i <= finishNum; i++) {
        result.push(i)
      }
    } else if (startNum > finishNum) {
      for(let i = startNum ; i >= finishNum; i--) {
        result.push(i)
      }
    }
  } else {
    return "-1"
  }
  return result;
}

console.log('----Range------','\n')
console.log(range(1, 10))  
console.log(range(1))   
console.log(range(11, 18))   
console.log(range(54, 50))   
console.log(range())   
console.log('\n')


console.log('Soal No. 2 (Range With Step)','\n') 
function rangeWithStep(startNum, finishNum, step) {
  let result = []
  if (startNum && finishNum) {
    if (startNum < finishNum) {
      for(let i = startNum ; i <= finishNum; i++) {
        let cek = step === 2 ? 1 : step === 3 ? 2 : '';
        if(i%step===cek){
          result.push(i)
        }
      }
    } else if (startNum > finishNum) {
      for(let i = startNum ; i >= finishNum; i--) {
        let cek = step === 1 ? 0 : step === 4 ? 1 : ''
        if(i%step === cek){
          result.push(i)
        }
      }
    }
  }
  return result;
}

console.log('----Range with step------','\n')
console.log(rangeWithStep(1, 10, 2))  
console.log(rangeWithStep(11, 23, 3))   
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4))   







console.log('\n \n \n Soal No. 4 (Array Multidimensi)','\n')
const dataHandling = () => {
  var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
  ]
  let result = []
  input.map(item => {
    let data = `
      Nomor ID:  ${item[0]}
      Nama Lengkap:  ${item[1]}
      TTL:  ${item[2]} ${item[3]}
      Hobi:  ${item[4]}
    `
    result.push(data)
    console.log(data)
  })
  return result;
}

dataHandling()

console.log('\n \n \n Soal No. 5 (Balik Kata)','\n')

const balikKata = (reverse) => {
  var splitString = reverse.split('');
  var reverseArray = splitString.reverse();
  var joinArray = reverseArray.join('');

 return joinArray;
}

console.log(balikKata("Kasur Rusak"))  
console.log(balikKata("SanberCode"))   
console.log(balikKata("Haji Ijah"))   
console.log(balikKata("racecar"))   
console.log(balikKata("I am Sanbers"))  




console.log('\n \n \n Soal No. 6 (Metode Array)','\n')

const dataHandling2 = () => {
const data = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
data.splice(1,1, "Roman Alamsyah Elsharawy" )
data.splice(2, 1, "Provinsi Bandar Lampung")
data.splice(4, 0, "Pria")
data.splice(5, 1, "SMA Internasional Metro")
console.log(data, '\n')
}

dataHandling2()
const data = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"] 
console.log(data)
console.log('\n \n \n \n \n')




console.log('Soal No. 3 (Sum of Range)','\n') 
function myFunc(total, num) {
    return total + num;
  }

function  sum(startNum, finishNum, step){
    if(startNum==null){
        return 0;
    }else if(finishNum==null){
        return startNum;
    }else if(startNum==null){
        return 0;
    }else if(step==null){
        var count=rangeWithStep(startNum, finishNum, 1);
        return count.reduce(function(a, b){
            return a + b;
            });
    }else {
        var count=rangeWithStep(startNum, finishNum, step);
        return count.reduce(function(a, b){
            return a + b;
            });
    }
}

console.log(sum(1,10))
console.log(sum(5, 50, 2))
console.log(sum(15,10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())