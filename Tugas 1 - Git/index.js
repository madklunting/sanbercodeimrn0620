//var sayHello = "Hello World!" 
//console.log(sayHello)

//var name = "Erinaku"// Tipe
//var angka = 13
//var todayIsFriday = true

//console.log(name) // "John"
//console.log(angka) // 12
//console.log(todayIsFriday) // false

//var angka = 100
//console.log(angka == 10) // true
//console.log(angka == 20) // false
//console.log(angka == 1000)
//console.log(angka == 100)


var tug1 = "\n Membuat Kalimat No.1 \n"
console.log(tug1) 

var word = "JavaScript ";
var second = "is";
var third = " awesome";
var fourth = " and";
var fifth = " I";
var sixth = " love";
var seventh = " it";
var res = word.concat(second, third, fourth, fifth, sixth, seventh);
console.log(res = word.concat(second, third, fourth, fifth, sixth, seventh));


var tug2 = "\n Mengurai Kalimat (Akses karakter dalam string) No.2 \n"
console.log(tug2)


var sentence = "I am going to be React Native Developer"; 

var firstWord = sentence[0] ; 
var secondWord = sentence[2] + sentence[3]  ; 
var thirdWord = sentence[4] + sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];  
var fourthWord = sentence[11] + sentence[12]; 
var fifthWord = sentence[14] + sentence[15]; 
var sixthWord = sentence[17] + sentence[18]+ sentence[19]+ sentence[20]+ sentence[21];  
var seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];  
var eighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];  

console.log('First Word: ' + firstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)

var tug3 = "\n Mengurai kalimat (Substring) No.3 \n"
console.log(tug3)

var sentence2 = 'wow JavaScript is so cool'; 

var firstWord2 = sentence2.substring(0, 3);
var secondWord2 = sentence2.substring(4, 14);
var thirdWord2 = sentence2.substring(15, 17);
var fourthWord2 = sentence2.substring(18, 20);
var fifthWord2 = sentence2.substring(21, 26);

console.log('First Word: ' + firstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

var tug4 = "\n Mengurai Kalimat dan Mnentukan Panjang String No.4 \n"
console.log(tug4)

var sentence3 = 'wow JavaScript is so cool'; 

var firstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14);
var thirdWord3 = sentence3.substring(15, 17);
var fourthWord3 = sentence3.substring(18, 20);
var fifthWord3 = sentence3.substring(21, 26);

var firstWordLength = firstWord3.length   
console.log('First Word: ' + firstWord3 + ', with length: ' + firstWordLength); 
var secondWordLength = secondWord3.length
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
var thirdWordLength = thirdWord3.length
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
var fourthWordLength = fourthWord3.length
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
var fifthWordLength = fifthWord3.length
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 