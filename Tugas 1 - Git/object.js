console.log('---------------Soal No. 1 (Array To Object)------------------','\n') 

function arrayToObject(arr) {
    var nama;
    var year = (new Date()).getFullYear();
    
    for (let i = 0; i < arr.length; i++) {
      var people = {};
      
      people.firstName = arr[i][0];
      people.lastName = arr[i][1];
      people.gender = arr[i][2];
    
      if (year > arr[i][3]) {
        people.age = year - arr[i][3];
      } else {
        people.age = 'Invalid Birth Year';
      }
      
      nama = (i+1) + '. ' + people.firstName + ' ' + people.lastName + ':\n';
      
      console.log(nama, people);
    }   
  }
  arrayToObject([["Bruce", "Banner", "male", 1975], ["Natasha", "Rommanoff", "female"]]);
  arrayToObject([["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]);
  arrayToObject([]);

  console.log('\n-----------Soal No. 2 (Shopping Time)-------------','\n')

class shop {
  constructor(memberID, money) {
    this.memberID = memberID;
    this.money = money;
    this.listPurchased = [];
    this.changeMoney = 0;
  }
}

function shoppingTime(memberId, money) {
  if (memberId === '' || (memberId === undefined && money === undefined)) {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else if (money < 50000) {
    return "Mohon maaf, uang tidak cukup";
  } else {
    shoppingObj = new shop(memberId, money);
    shoppingObj.memberID = memberId;
    shoppingObj.money = money;
    var canPurchase = true;
    while (money > 0 && canPurchase) {
      if (money >= 1500000) {
        shoppingObj.listPurchased.push('Sepatu Stacattu');
        money -= 1500000;
        canPurchase = true;
      }
      if (money >= 500000) {
        shoppingObj.listPurchased.push('Baju Zoro');
        money -= 500000;
        canPurchase = true;
      }
      if (money >= 250000) {
        shoppingObj.listPurchased.push('Baju H&N');
        money -= 250000;
        canPurchase = true;
      }
      if (money >= 175000) {
        shoppingObj.listPurchased.push('Sweater Uniklooh');
        money -= 175000;
        canPurchase = true;
      }
      if (money >= 50000) {
        shoppingObj.listPurchased.push('Casing Handphone');
        money -= 50000;
        canPurchase = true;
      }
      canPurchase = false;
    }
    shoppingObj.changeMoney = money;
    return shoppingObj;
  }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime());

console.log('\n---------Soal No. 3 (Naik Angkot)------------','\n')

function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  const biaya = 2000;
  var hasil = [];
  
  if (arrPenumpang.length === 0) {
    return arrPenumpang;
  }
  
  for (let i = 0; i < arrPenumpang.length; i++) {
    var penumpang = arrPenumpang[i];
    var objPenumpang = {};
    
    objPenumpang.penumpang = penumpang[0];
    objPenumpang.naikDari = penumpang[1];
    objPenumpang.tujuan = penumpang[2];
    objPenumpang.bayar = biaya * (rute.indexOf(objPenumpang.tujuan) - rute.indexOf(objPenumpang.naikDari));
    
    hasil.push(objPenumpang);
  }
  
  return hasil;
}


console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); 


